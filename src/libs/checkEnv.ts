import { Collection } from "discord.js";

function parseEnv() {
  if (!process.env.DISCORD_TOKEN) {
    console.log("ERROR! missing 'DISCORD_TOKEN' in .env file");
    process.exit();
  }

  if (!process.env.PRIMARY_ADULT_ROLE) {
    console.log("ERROR! missing 'PRIMARY_ADULT_ROLE' in .env file");
    process.exit();
  }

  if (!process.env.OTHER_ADULT_ROLES) {
    console.log("ERROR! missing 'OTHER_ADULT_ROLES' in .env file");
    process.exit();
  }
  let roles: string[];
  try {
    roles = Array(...JSON.parse(process.env.OTHER_ADULT_ROLES));
  } catch (error) {
    console.log("ERROR! could not parse 'OTHER_ADULT_ROLES' as array");
    process.exit();
  }

  let config = {
    token: process.env.DISCORD_TOKEN,
    adultRole: process.env.PRIMARY_ADULT_ROLE,
    syncRoles: roles,
  };

  return config;
}

export default parseEnv;
